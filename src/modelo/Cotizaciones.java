
package modelo;

/**
 *
 * @author quier
 */
public class Cotizaciones {
    private int idCotizacion;
    private String numCotizacion;
    private String descripcionAutomovil; 
    private int precioAutomovil;
    private int porcentajePago;
    private int plazo;

    public Cotizaciones(int idCotizacion, String numCotizacion, String descripcionAutomovil, int precioAutomovil, int porcentajePago, int plazo) {
        this.idCotizacion = idCotizacion;
        this.numCotizacion = numCotizacion;
        this.descripcionAutomovil = descripcionAutomovil;
        this.precioAutomovil = precioAutomovil;
        this.porcentajePago = porcentajePago;
        this.plazo = plazo;
    }

    public Cotizaciones(Cotizaciones otro) {
        this.idCotizacion = otro.idCotizacion;
        this.numCotizacion = otro.numCotizacion;
        this.descripcionAutomovil = otro.descripcionAutomovil;
        this.precioAutomovil = otro.precioAutomovil;
        this.porcentajePago = otro.porcentajePago;
        this.plazo = otro.plazo;
    }
    
    public Cotizaciones() {
        this.idCotizacion = 0;
        this.numCotizacion = "";
        this.descripcionAutomovil = "";
        this.precioAutomovil = 0;
        this.porcentajePago = 0;
        this.plazo = 0;
    }
    
    public int getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(int idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public String getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(String numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public int getPrecioAutomovil() {
        return precioAutomovil;
    }

    public void setPrecioAutomovil(int precioAutomovil) {
        this.precioAutomovil = precioAutomovil;
    }

    public int getPorcentajePago() {
        return porcentajePago;
    }

    public void setPorcentajePago(int porcentajePago) {
        this.porcentajePago = porcentajePago;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    // Métodos de comportamiento
    public float calcularPagoInicial() {
        return this.precioAutomovil * this.porcentajePago / 100;
    }
    
    public float calcularTotalAFinanciar() {
        return this.precioAutomovil - this.calcularPagoInicial();
    }
    
    public float calcularPagoMensual() {
        return this.calcularTotalAFinanciar()/ this.plazo;
    }
    
    public void imprimirCotizacion() {
        System.out.println("======= TOYOTA MAZATLAN =======");
        System.out.println("");
        System.out.println("Numero de cotizacion: " +this.numCotizacion);
        System.out.println("Descripcion de Automovil: " + this.descripcionAutomovil);
        System.out.println("El costo de Automovil es: " + this.precioAutomovil);
        System.out.println("Pago Inicial del Automovil es: " + this.calcularPagoInicial());
        System.out.println("Total a Financiar es: " + this.calcularTotalAFinanciar());
        System.out.println("El Pago Mensual es: " + this.calcularPagoMensual());
        System.out.println("");
        
    }
    
}
